<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//FRONTPAGE
Route::get('/', 'FrontController@inicio');
Route::get('/ofertas', 'FrontController@ofertas');
Route::get('/nosotros', 'FrontController@nosotros');
Route::get('/servicios', 'FrontController@servicios');
Route::get('/auto/{id}', 'FrontController@verAuto');
Route::post('/contactar', ['as' => 'front.contactar', 'uses' => 'FrontController@contactar']);

//Auto
Route::resource('app/auto', 'AutoController');
Route::get('/app/images/{idAuto}', 'AutoController@images');
Route::post('app/images', ['as' => 'images.store', 'uses' => 'AutoController@uploadImages']);
Route::delete('/app/image/{idAuto}/{idImage}', 'AutoController@destroyImage');
Route::post('/app/image/main/{idAuto}/{idImage}', 'AutoController@isMainImage');

//User
Route::get('/app', 'UserController@showLogin');
Route::post('app/user/login', ['as' => 'user.login', 'uses' => 'UserController@login']);
Route::get('logout', 'UserController@logout');



