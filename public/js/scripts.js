$(document).ready(function(){
	var nFile = 0;	
	$('input[type=file]').change(function () {
          console.log(this.files);
          $('#thumbs').empty(); 

	      nFile = this.files.length;
	      if (nFile == 1){
	      	$('#info-input-file').html(this.files[0].name);
	      	//console.log(this.files[0]);
	      } else if (nFile > 1) {
	      	$('#info-input-file').html(nFile + " imágenes seleccionadas");
	      }
	      
	      for (var i = 0; i <  nFile; i++) {
		    var fr = new FileReader();
		    fr.onload = function(e) {
		      $('#thumbs').append(addItemImage(e.target.result));
		    }
		    fr.readAsDataURL(this.files[i]);
		  }
	})

    function addItemImage(src){

        return imageItem = "<img  src='"+ src +"'>";

    }

	
    var carTable = $('#mainTable').DataTable({
    	"ordering": false,
    	"bLengthChange": false,
    	"searching": true,
        "responsive": true,
    	"language": {
            "lengthMenu": "Mostrar _MENU_ modelos por página",
            "zeroRecords": "No hay registros",
            "info": "_PAGE_ de _PAGES_",
            "infoEmpty": "No hay registros disponibles",
            "infoFiltered": "(Filtrado desde _MAX_ registros)","sInfoPostFix":   "",
            "sSearch":        "Buscar:",
            "sUrl":           "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":    "Último",
                "sNext":    "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }

        }
    });


	$('#search-car').on( 'keyup', function () {
    	carTable.search( this.value ).draw();
	});

    $('.b-popup')
      .popup({
        on: 'hover'
    });

    $('.view-btn').click(function(){
        $('#see-modal').modal('show');
    });  

    $('.delete-btn').click(function(){
        $('#delete-modal').modal('show');
    }); 

    $('.delete-btn-img').click(function(){
        $('#delete-modal-img').modal('show');
    }); 

    $('.main-btn-img').click(function(){
        $('#main-modal-img').modal('show');
    }); 

    $('select.dropdown')
    .dropdown();
	
    
});
