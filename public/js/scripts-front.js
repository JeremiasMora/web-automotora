$(document).ready(function(){

	$('.carousel').flickity({
		  // options
		autoPlay: 3000
	});

	$(window).load(function(){
      $('.preloader').fadeOut(1000); // set duration in brackets    
    });

    $('#navbar3').navScroll();

    $(".precio-oferta").each(function(){
		$(this).text(  numberWithCommas($(this).text())  );
	 }); 
    $(".normal-precio").each(function(){
		$(this).text(  numberWithCommas($(this).text())  );
	 }); 

	$("#lightgallery").lightGallery();

	function numberWithCommas(x) {
    	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
	}
    
});
