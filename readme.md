## Automotora WebApp

Aplicación web para la gestión de automóviles y su publicación en sitio web.

###Ejecutar proyecto

- Crear base de datos MySQL o Postgresql
- Crear y configurar .env
- Ejecutar proyecto

```
composer install
composer dump-autoload

php artisan migrate --seed
php artisan serve
php artisan key:generate
php artisan vendor:publish
php artisan cache:clear
```