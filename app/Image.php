<?php

namespace Automotora;
use Automotora\Auto;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $table = 'images';

    protected $fillable = [
        'path',
        'auto_id',
    ];

    public static $rules = [
    	//'user_id'  => "required",
    	'path'  => 'required|image|mimes:jpeg,png,jpg|max:8048',
    ];

    public static $niceNames = [
        'path'  => 'seleccionar imágenes',
    ];

    public function auto(){
    	return $this->belongsTo(Auto::class);
    }
}
