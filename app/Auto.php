<?php

namespace Automotora;

use Illuminate\Database\Eloquent\Model;
use Automotora\Image;

class Auto extends Model
{
    protected $table = 'autos';

    protected $fillable = [
    'aire_acondicionado',
    'alza_vid_elect',
    'frenos_abs',
		'airbag',
		'cierre_centralizado',
		'catalitico',
		'llantas',
		'espejos_elect',
		'unico_dueno',
		'alarma',
		'radio',
		'ctrl_crucero',
		'ctrl_estabilidad',
		'agno',
		'cant_airbag',
		'consumo_combustible',
		'kilometraje',
		'cilindrada',
		'des',
		'marca',
		'modelo',
		'version',
		'precio',
		'precio_oferta',
		'en_oferta',
		'main_image',
		'combustible_id',
		'tipo_id',
    ];

    public static $rules = [
    	'marca'  => "required|max:255",
    	'modelo'  => "required|max:255",
    	'version'  => "required|max:255",
    	'agno'  => "required|max:100",
      'des'   => 'max:255',
    ];

    public static $niceNames = [
        'des'  => 'descripción',
        'version'  => "versión",
        'agno'  => "año",
    ];

    //cada auto puede tener varios registros relacionados
    // en image, change name
    public function images(){
    	return $this->hasMany(Image::class);
    }
}
