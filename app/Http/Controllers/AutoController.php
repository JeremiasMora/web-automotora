<?php

namespace Automotora\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\File;
use Automotora\Auto;
use Automotora\Tipo;
use Automotora\Combustible;
use Automotora\Image;

class AutoController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $autos = Auto::orderBy('created_at', 'desc')->with('images')->get();

        return view('auto.index', compact('autos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipos = Tipo::pluck('nombre', 'id');
        $combustibles = Combustible::pluck('nombre', 'id');
        return view('auto.create', compact('tipos', 'combustibles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $this->validate($request, Auto::$rules, [], Auto::$niceNames);

        if ($request['aire_acondicionado'] == null) { 
            $request['aire_acondicionado'] = 0; 
        }
        if ($request['alza_vid_elect'] == null) { 
            $request['alza_vid_elect'] = 0; 
        }
        if ($request['frenos_abs'] == null) { 
            $request['frenos_abs'] = 0; 
        }
        if ($request['airbag'] == null) { 
            $request['airbag'] = 0; 
        }
        if ($request['cierre_centralizado'] == null) { 
            $request['cierre_centralizado'] = 0; 
        }
        if ($request['catalitico'] == null) { 
            $request['catalitico'] = 0; 
        }
        if ($request['espejos_elect'] == null) { 
            $request['espejos_elect'] = 0; 
        }
        if ($request['unico_dueno'] == null) { 
            $request['unico_dueno'] = 0; 
        }
        if ($request['radio'] == null) { 
            $request['radio'] = 0; 
        }
        if ($request['ctrl_crucero'] == null) { 
            $request['ctrl_crucero'] = 0; 
        }
        if ($request['ctrl_estabilidad'] == null) { 
            $request['ctrl_estabilidad'] = 0; 
        }
        if ($request['en_oferta'] == null) { 
            $request['en_oferta'] = 0; 
        }


        
        Auto::create([
            'aire_acondicionado' => $request['aire_acondicionado'],
            'alza_vid_elect' => $request['alza_vid_elect'],
            'frenos_abs' => $request['frenos_abs'],
            'airbag' => $request['airbag'],
            'cierre_centralizado' => $request['cierre_centralizado'],
            'catalitico' => $request['catalitico'],
            'espejos_elect' => $request['espejos_elect'],
            'unico_dueno' => $request['unico_dueno'],
            'radio' => $request['radio'],
            'ctrl_crucero' => $request['ctrl_crucero'],
            'ctrl_estabilidad' => $request['ctrl_estabilidad'],
            'agno' => $request['agno'],
            'cant_airbag' => $request['cant_airbag'],
            'consumo_combustible' => $request['consumo_combustible'],
            'kilometraje' => $request['kilometraje'],
            'cilindrada' => $request['cilindrada'],
            'des' => $request['des'],
            'marca' => $request['marca'],
            'modelo' => $request['modelo'],
            'version' => $request['version'],
            'precio' => $request['precio'],
            'precio_oferta' => $request['precio_oferta'],
            'en_oferta' => $request['en_oferta'],
            'main_image' => 0,
            'combustible_id' => $request['combustible_id'],
            'tipo_id' => $request['tipo_id'],
        ]);

        return redirect('/app/auto/')->with('msg', "Se ha creado el registro correctamente");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipos = Tipo::pluck('nombre', 'id');
        $combustibles = Combustible::pluck('nombre', 'id');
        $auto = Auto::find($id);
        return view('auto.edit', compact('auto','tipos', 'combustibles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, Auto::$rules, [], Auto::$niceNames);

        $auto = Auto::find($id);

        if ($request['aire_acondicionado'] == null) { 
            $request['aire_acondicionado'] = 0; 
        }
        if ($request['alza_vid_elect'] == null) { 
            $request['alza_vid_elect'] = 0; 
        }
        if ($request['frenos_abs'] == null) { 
            $request['frenos_abs'] = 0; 
        }
        if ($request['airbag'] == null) { 
            $request['airbag'] = 0; 
        }
        if ($request['cierre_centralizado'] == null) { 
            $request['cierre_centralizado'] = 0; 
        }
        if ($request['catalitico'] == null) { 
            $request['catalitico'] = 0; 
        }
        if ($request['espejos_elect'] == null) { 
            $request['espejos_elect'] = 0; 
        }
        if ($request['unico_dueno'] == null) { 
            $request['unico_dueno'] = 0; 
        }
        if ($request['radio'] == null) { 
            $request['radio'] = 0; 
        }
        if ($request['ctrl_crucero'] == null) { 
            $request['ctrl_crucero'] = 0; 
        }
        if ($request['ctrl_estabilidad'] == null) { 
            $request['ctrl_estabilidad'] = 0; 
        }
        if ($request['en_oferta'] == null) { 
            $request['en_oferta'] = 0; 
        }


        $auto->fill([
            'aire_acondicionado' => $request['aire_acondicionado'],
            'alza_vid_elect' => $request['alza_vid_elect'],
            'frenos_abs' => $request['frenos_abs'],
            'airbag' => $request['airbag'],
            'cierre_centralizado' => $request['cierre_centralizado'],
            'catalitico' => $request['catalitico'],
            'espejos_elect' => $request['espejos_elect'],
            'unico_dueno' => $request['unico_dueno'],
            'radio' => $request['radio'],
            'ctrl_crucero' => $request['ctrl_crucero'],
            'ctrl_estabilidad' => $request['ctrl_estabilidad'],
            'agno' => $request['agno'],
            'cant_airbag' => $request['cant_airbag'],
            'consumo_combustible' => $request['consumo_combustible'],
            'kilometraje' => $request['kilometraje'],
            'cilindrada' => $request['cilindrada'],
            'des' => $request['des'],
            'marca' => $request['marca'],
            'modelo' => $request['modelo'],
            'version' => $request['version'],
            'precio' => $request['precio'],
            'precio_oferta' => $request['precio_oferta'],
            'en_oferta' => $request['en_oferta'],
            'combustible_id' => $request['combustible_id'],
            'tipo_id' => $request['tipo_id'],
        ]);

        $auto->save();

        return redirect('/app/auto/')->with('msg', "Se ha editado  correctamente");
    }

    public function images($id)
    {
        $auto = Auto::find($id);
        $images = $auto->images;
        $idMainImage = $auto->main_image;
        return view('auto.image', compact('auto', 'images', 'idMainImage'));
    }

    public function uploadImages(Request $request){

        $rules = [];
        $niceNames = [];
        $files = $request->path;
        $pathc = count($files) - 1;

        if (count($files) > 0){
            foreach(range(0, $pathc) as $index) {
                $rules['path.' . $index] = 'required|image|mimes:jpeg,png,jpg|max:8048';
                $niceNames['path.' . $index] = $files[$index]->getClientOriginalName();
            }
            $this->validate($request, $rules, [], $niceNames);
        } else {
            $this->validate($request, Image::$rules, [], Image::$niceNames);
        }

        foreach($files as $file){
            $img_path = \Storage::disk('s3')->putFile('images', new File($file));  
            Image::create([
                'auto_id' => $request['idAuto'],
                'path' => $img_path,
            ]);
        }
        return redirect('/app/images/'.$request['idAuto'])->with('msg', "Proceso realizado correctamente");
    }

    public function destroyImage($idAuto, $idImage){
        $auto = Auto::find($idAuto);
        if ($auto->main_image != $idImage){
            $imagePath = Image::Find($idImage)->path;
            Image::destroy($idImage);
            \Storage::disk('s3')->delete($imagePath);
        }
        return 'ok';
    }

    public function isMainImage($idAuto, $idImage){
        $auto = Auto::find($idAuto);
        $auto->fill([
            'main_image' => $idImage,
        ]);

        $auto->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $auto = Auto::where('id', $id)->with('images')->get();  
        $images = $auto[0]->images;
        foreach ($images as $image){
            \Storage::disk('s3')->delete($image->path);
        }
        Auto::destroy($id);
        return 'ok';
    }
}
