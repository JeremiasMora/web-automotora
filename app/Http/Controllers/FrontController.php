<?php

namespace Automotora\Http\Controllers;

use Illuminate\Http\Request;
use Automotora\Auto;
use Automotora\Combustible;
use Automotora\Tipo;
use Automotora\Image;
use Input;
use Mail;

class FrontController extends Controller
{
    function inicio(){
        $autos = Auto::orderBy('created_at', 'desc')->with('images')->where('en_oferta', false)->paginate(15);
        $autosOferta = Auto::orderBy('created_at', 'desc')->with('images')->where('en_oferta', true)->get();
        return view('front.inicio', compact('autos', 'autosOferta'));
    }

    function verAuto($id){  
        $auto = Auto::find($id);
        $images = $auto->images;
        $combustible = Combustible::find($auto->combustible_id)->nombre;
        $tipo = Tipo::find($auto->tipo_id)->nombre;
        return view('front.auto', compact('auto', 'images', 'combustible', 'tipo'));
    }

    function contactar(Request $request){ 

        Mail::send('email.consignacion', 
            [
                'nombre' => $request->nombre, 
                'correo' => $request->correo,
                'telefono' => $request->telefono,
                'marca' => $request->marca,
                'modelo' => $request->modelo,
                'agno' => $request->agno,
                'descripcion' => $request->descripcion,
            ], function ($message) {
            $message->to('aliagautomotriz@gmail.com', Input::get('nombre'))
                ->subject('Solicitud de consignación');
        });
        return redirect('/')->with('msg', "Se ha enviado el correo exitosamente");
    }

    function ofertas(){
        $autosOferta = Auto::orderBy('created_at', 'desc')->with('images')->where('en_oferta', true)->get();
    	return view('front.ofertas', compact('autosOferta'));
    }

    function nosotros(){
    	return view('front.nosotros');
    }

    function servicios(){
    	return view('front.servicios');
    }
}
