<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


class CreateAutosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autos', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('aire_acondicionado');
            $table->boolean('alza_vid_elect');
            $table->boolean('frenos_abs');
            $table->boolean('airbag');
            $table->boolean('cierre_centralizado');
            $table->boolean('catalitico');
            $table->boolean('espejos_elect');
            $table->boolean('unico_dueno');
            $table->boolean('radio');
            $table->boolean('ctrl_crucero');
            $table->boolean('ctrl_estabilidad');
            $table->integer('agno');
            $table->integer('cant_airbag');
            $table->integer('consumo_combustible');
            $table->integer('kilometraje');
            $table->integer('cilindrada');
            $table->string('des');
            $table->string('marca');
            $table->string('modelo');
            $table->string('version');
            $table->integer('precio');
            $table->integer('precio_oferta');
            $table->boolean('en_oferta');
            $table->integer('main_image');

            $table->integer('combustible_id')->unsigned();
            $table->foreign('combustible_id')->references('id')->on('combustibles');

            $table->integer('tipo_id')->unsigned();
            $table->foreign('tipo_id')->references('id')->on('tipos');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autos');
    }
}
