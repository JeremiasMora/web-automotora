<?php

use Illuminate\Database\Seeder;
use Automotora\Combustible;

class CombustiblesTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Combustible::updateOrCreate(['nombre' => 'Diesel']);
    Combustible::updateOrCreate(['nombre' => 'Bencina']);
  }
}
