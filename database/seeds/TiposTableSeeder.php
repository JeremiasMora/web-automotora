<?php

use Illuminate\Database\Seeder;
use Automotora\Tipo;

class TiposTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	Tipo::updateOrCreate(['nombre' => 'Sedan']);
    	Tipo::updateOrCreate(['nombre' => 'Citycar']);
    	Tipo::updateOrCreate(['nombre' => 'SUV']);
    	Tipo::updateOrCreate(['nombre' => 'Station Wagon']);
    	Tipo::updateOrCreate(['nombre' => 'Furgon']);
    	Tipo::updateOrCreate(['nombre' => 'Hatchback']);
    	Tipo::updateOrCreate(['nombre' => 'Sport']);
    	Tipo::updateOrCreate(['nombre' => 'Coupe']);
    	Tipo::updateOrCreate(['nombre' => 'Camioneta']);
    	Tipo::updateOrCreate(['nombre' => 'Camion 3/4']);
    	Tipo::updateOrCreate(['nombre' => 'Limosina']);
    	Tipo::updateOrCreate(['nombre' => 'Mini Bus']);

    }
}
