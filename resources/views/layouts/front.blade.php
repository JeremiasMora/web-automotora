<!DOCTYPE html>

<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" sizes="180x180" href="ico/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="aliaga-favicon.png">
    <link rel="icon" type="image/png" sizes="16x16" href="aliaga-favicon.png">
    <link rel="manifest" href="ico/manifest.json">
    <link rel="mask-icon" href="ico/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <meta name="keywords" content="Automotríz Aliaga, Temuco Regíón de la Araucanía.">
    <meta name="description" content="Empresa automotriz Aliaga de Temuco, región de la Araucanía."/>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/css/iziModal.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    @if (getenv('APP_ENV') === 'production')
      <link href="{{ secure_asset('css/btncss.css') }}" media="all" rel="stylesheet" type="text/css"/>
      <link href="{{ secure_asset('css/lightgallery.min.css') }}" media="all" rel="stylesheet" type="text/css" />
      <link href="{{ secure_asset('css/styles-front.css') }}" media="all" rel="stylesheet" type="text/css" />
    @elseif (getenv('APP_ENV') === 'local')
      {!!Html::style('css/btncss.css')!!}
      {!!Html::style('css/lightgallery.min.css')!!}
      {!!Html::style('css/styles-front.css')!!}
    @endif
    <title>Automotriz Aliaga | Temuco</title>
  </head>

  <body>
    <!-- <div class="preloader">
      <div class="sk-spinner sk-spinner-rotating-plane"></div>
    </div> -->

    <div id="menu-space"></div>
  
    <div id="main-menu">
      <nav class="navbar navbar-static-top">
        <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#"><img src="aliaga.png" alt="Dispute Bills" style="width: 130px;height: 107px; margin-top: -13px">
            </a>
          </div>
          <div id="navbar3" class="navbar-collapse collapse nav-d">
            <ul class="nav navbar-nav navbar-right">
              <li class="{{ Request::path() == '/' ? 'active' : '' }}"><a href="{{ url('/') }}">INICIO</a></li>
               <li class="{{ Request::path() == 'ofertas' ? 'active' : '' }}"><a href="{{ url('/ofertas') }}">OFERTAS</a></li> 
              <li class="{{ Request::path() == 'nosotros' ? 'active' : '' }}"><a href="{{ url('/nosotros') }}">NOSOTROS</a></li>
              <li class="{{ Request::path() == 'servicios' ? 'active' : '' }}"><a href="{{ url('/servicios') }}">SERVICIOS</a></li>
              <li><a href="#footer">CONSIGNACIÓN</a></li>
            </ul>
          </div>
          <!--/.nav-collapse -->
        </div>
        <!--/.container-fluid -->
      </nav>
    </div>

    

    @yield('content')


    <footer id="footer">
      <div class="container">
        <div class="row">
          <div class="col-md-6">
            <h3 class="title-border-1">Consigna tu vehículo aquí</h3>

            <div id="form">
            {!!Form::open(['route'=>'front.contactar', 'method'=>'POST', 'class' => 'form'])!!}
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="name">Nombre:</label>
                    {!!Form::text('nombre', null,['class'=>'form-control'])!!}
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="email">Correo electrónico:</label>
                    {!!Form::email('correo', null,['class'=>'form-control'])!!}
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="phone">Número de teléfono:</label>
                    {!!Form::text('telefono', null,['class'=>'form-control'])!!}
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="marca">Marca:</label>
                    {!!Form::text('marca', null,['class'=>'form-control'])!!}
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="modelo">Modelo:</label>
                    {!!Form::text('modelo', null,['class'=>'form-control'])!!}
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="agno">Año:</label>
                    {!!Form::number('agno', null,['class'=>'form-control'])!!}
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label for="des">Descripción:</label>
                {{ Form::textarea('descripcion', null, ['size' => '30x5', 'class'=>'form-control']) }}
              </div>
              {!!Form::submit('ENVIÁR', ['class'=>'bttn-fill bttn-md bttn-danger pull-right'])!!}
            {!!Form::close()!!}
            </div>

          </div>
          
          <div id="footer-separator" class="hidden-lg hidden-md" style="height: 20px">
            
          </div>

          <div class="col-md-6" id="data-contact">
            <h3 class="title-border-2">Contáctanos</h3>
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1556.0983114512587!2d-72.59646761180059!3d-38.736249997411115!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMzjCsDQ0JzEwLjUiUyA3MsKwMzUnNDQuNSJX!5e0!3m2!1ses!2scl!4v1511470361187" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
            <div id="contact-list">
              <ul>
                <li>
                  <i class="fa fa-envelope"></i> <a href="mailto:aliagautomotriz@gmail.com" target="_blank"> aliagautomotriz@gmail.com</a>
                </li>
                <li>
                  <i class="fa fa-phone"></i> +569 90512424 
                </li>
                <li>
                  <i class="fa fa-map-marker"></i> Portales 385, esquina Las Heras - Temuco.
                </li>
              </ul>
            </div>
          </div>
        </div>
        <div id="end-sign">
          <div class="float-left col-md-6">
            <p>Sitio Web Oficial Automotriz Aliaga </p> 
          </div>
          <div class="col-md-6">
            <p class="pull-right ">
              <a href="https://www.motriz.online" target="_blank" style="color:white; text-decoration: none;">Creado por Motriz.online</a>
            </p>
          </div>
        </div>
      </div>
    </footer>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.min.js"></script>

  @if (getenv('APP_ENV') === 'production')
    <script src="{{ secure_asset('js/lightgallery.min.js') }}"></script>
    <script src="{{ secure_asset('js/lg-fullscreen.min.js') }}"></script>
  @elseif (getenv('APP_ENV') === 'local')
    {!!Html::script('js/lightgallery.min.js')!!}
    {!!Html::script('js/lg-fullscreen.min.js')!!}
  @endif 

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  <script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/izimodal/1.5.1/js/iziModal.min.js"></script>

  @if (getenv('APP_ENV') === 'production')
      <script src="{{ secure_asset('js/jquery.navScroll.min.js') }}"></script> 
      <script src="{{ secure_asset('js/scripts-front.js') }}"></script>
  @elseif (getenv('APP_ENV') === 'local')
    {!!Html::script('js/jquery.navScroll.min.js')!!}
    {!!Html::script('js/scripts-front.js')!!}
  @endif

  
  </body>
</html>