<!DOCTYPE html>

<html lang="es">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @if (getenv('APP_ENV') === 'production')
      <link href="{{ secure_asset('css/styles.css') }}" media="all" rel="stylesheet" type="text/css" />
    @elseif (getenv('APP_ENV') === 'local')
      {!!Html::style('css/styles.css')!!}
    @endif
 
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css">

    @if (getenv('APP_ENV') === 'production')
      <link href="{{ secure_asset('css/dataTables.semanticui.min.css') }}" media="all" rel="stylesheet" type="text/css" />
    @elseif (getenv('APP_ENV') === 'local')
      {!!Html::style('css/dataTables.semanticui.min.css')!!}
    @endif
    <title>Miguel Aliaga Adm</title>
  </head>

  <body>

    <!--HEADER-->
    <div class="header-color">
      <div class="ui container">
        <div class="ui secondary menu smenu">
          <div class="header item">
            <a href="{{ url('app/auto') }}" style="color: #fff">
              Automotriz Aliaga
            </a>
          </div>
            <a class="active item" href="{{ url('app/auto') }}">
              <i class="icon car"></i>
              Automóviles
            </a>
            <a class="item" href="{{ url('/') }}" target="_blank">
              <i class="icon pointing right"></i>
              Ver sitio web
            </a>
            <div class="right menu">
              <span class="ui item">
                <i class="icon user"></i>
                {!! Auth::user()->username !!}
              </span>
              <a href="/logout" class="ui item">
                <i class="icon sign out"></i>
                   Cerrar sesión
              </a>
            </div>
        </div>
      </div>
    </div>

    <!-- BODY 
    <div class="ui container">
       
    </div>/BODY --> 

    @yield('content')


  <script
  src="https://code.jquery.com/jquery-3.1.1.min.js"
  integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
  crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.js"></script>
  
  @if (getenv('APP_ENV') === 'production')
    <script src="{{ secure_asset('js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ secure_asset('js/dataTables.semanticui.min.js') }}"></script>
    <script src="{{ secure_asset('js/scripts.js') }}"></script>
  @elseif (getenv('APP_ENV') === 'local')
    {!!Html::script('js/jquery.dataTables.min.js')!!}
    {!!Html::script('js/dataTables.semanticui.min.js')!!}
    {!!Html::script('js/scripts.js')!!}
  @endif
  </body>
</html>