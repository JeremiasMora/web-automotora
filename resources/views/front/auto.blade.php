@extends('layouts.front')

@section('content')

<section id="individual-car">
  <div class="container">
    <div id="individual-car-title">
      <h3>{!! $auto->marca !!} {!! $auto->modelo !!} {!! $auto->version !!}</h3>
      @if($auto->en_oferta)
        <h5><span>$</span> {!! $auto->precio_oferta !!}</h5>
      @else
        <h5><span>$</span> {!! $auto->precio !!}</h5>
      @endif
      
      <p>{!! $auto->des !!}</p>
    </div>
    
    <div class="row" id="fast-contact">
      
        <div class="col-md-4 fast-contact-item">
          <i class="fa fa-phone"></i> +569 90512424
        </div>
        <div class="col-md-4 fast-contact-item">
          <i class="fa fa-envelope"></i> <a href="mailto:aliagautomotriz@gmail.com" target="_blank"> aliagautomotriz@gmail.com</a>
        </div>
        <div class="col-md-4 fast-contact-item">
          <i class="fa fa-map-marker"></i> <a href="">Portales 385, esquina Las Heras - Temuco</a>
        </div>
      
    </div>

    <div class="row">

      <div id="lightgallery" class="col-md-6">
        @foreach($images as $image)
          <a href="https://s3.us-east-1.amazonaws.com/aliagautomotriz/{!! $image->path !!}">
              <img src="https://s3.us-east-1.amazonaws.com/aliagautomotriz/{!! $image->path !!}" />
          </a>
        @endforeach
      </div>

      <div id="individual-esp" class="col-md-6">
        <h4>ESPECIFICACIONES</h4>
        <div id="esp-table">
          <table class="table">
            <tbody>
              <tr>
                <td class="con">Año</td>
                <td class="def">{{ $auto->agno }}</td>
              </tr>
              <tr>
                <td class="con">Marca</td>
                <td class="def">{{ $auto->marca }}</td>
              </tr>
              <tr>
                <td class="con">Modelo</td>
                <td class="def">{{ $auto->modelo }}</td>
              </tr>
              <tr>
                <td class="con">Versión</td>
                <td class="def">{{ $auto->version }}</td>
              </tr>
              <tr>
                <td class="con">Aire acondicionado</td>
                <td class="def">
                  @if($auto->aire_acondicionado)
                    Si
                  @else
                    No
                  @endif    
                </td>
              </tr>
              <tr>
                <td class="con">Alza vidrios electrónicos</td>
                <td class="def">
                  @if($auto->alza_vid_elect)
                    Si
                  @else
                    No
                  @endif    
                </td>
              </tr>
              <tr>
                <td class="con">Frenos ABS</td>
                <td class="def">
                  @if($auto->frenos_abs)
                    Si
                  @else
                    No
                  @endif    
                </td>
              </tr>
              <tr>
                <td class="con">Airbag</td>
                <td class="def">
                  @if($auto->airbag)
                    Si
                  @else
                    No
                  @endif    
                </td>
              </tr>
              <tr>
                <td class="con">Cierre centralizado</td>
                <td class="def">
                  @if($auto->cierre_centralizado)
                    Si
                  @else
                    No
                  @endif    
                </td>
              </tr>
              <tr>
                <td class="con">Catalítico</td>
                <td class="def">
                  @if($auto->catalitico)
                    Si
                  @else
                    No
                  @endif    
                </td>
              </tr>
              <tr>
                <td class="con">Espejos electrónicos</td>
                <td class="def">
                  @if($auto->espejos_elect)
                    Si
                  @else
                    No
                  @endif    
                </td>
              </tr>
              <tr>
                <td class="con">Único dueño</td>
                <td class="def">
                  @if($auto->unico_dueno)
                    Si
                  @else
                    No
                  @endif    
                </td>
              </tr>
              <tr>
                <td class="con">Control de estabilidad</td>
                <td class="def">
                  @if($auto->ctrl_estabilidad)
                    Si
                  @else
                    No
                  @endif    
                </td>
              </tr>
              <tr>
                <td class="con">Radio</td>
                <td class="def">
                  @if($auto->radio)
                    Si
                  @else
                    No
                  @endif    
                </td>
              </tr>
              <tr>
                <td class="con">Control crucero</td>
                <td class="def">
                  @if($auto->ctrl_crucero)
                    Si
                  @else
                    No
                  @endif    
                </td>
              </tr>
              <tr>
                <td class="con">Cantidad Airbag</td>
                <td class="def">{{ $auto->cant_airbag }}</td>
              </tr>
              <tr>
                <td class="con">Consumo por litro</td>
                <td class="def">{{ $auto->consumo_combustible }}</td>
              </tr>
              <tr>
                <td class="con">Kilometraje</td>
                <td class="def">{{ $auto->kilometraje }}</td>
              </tr>
              <tr>
                <td class="con">Cilindrada</td>
                <td class="def">{{ $auto->cilindrada }}</td>
              </tr>
              <tr>
                <td class="con">Combustible</td>
                <td class="def">{{ $combustible }}</td>
              </tr>
              <tr>
                <td class="con">Tipo</td>
                <td class="def">{{ $tipo }}</td>
              </tr>


            </tbody>
          </table>
        </div>
      </div>
    
    </div>

  </div>
</section>

@stop 