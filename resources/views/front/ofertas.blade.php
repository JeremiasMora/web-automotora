@extends('layouts.front')

@section('content')

<header id="second-header">
  <div class="container">
    <h2>Ofertas</h2>
  </div>
</header>
    
<div id="data-cars">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h3>LAS MEJORES <span>OFERTAS</span></h3>
        <div class="head-icon hidden-xs">
          <img src="img/head-icon.gif" alt="">
        </div>
      </div>
    </div>

    <div id="list-cars" class="row">

      @foreach($autosOferta as $auto)
       
          <div class="col-md-4 col-sm-6">
            <div class="card-car" style="height: 440px;">

              @if ($auto->main_image != 0)
                @foreach($auto->images as $image)
                  @if ($auto->main_image == $image->id)
                    <img src="https://s3.us-east-1.amazonaws.com/aliagautomotriz/{!! $image->path !!}" class="img-responsive">
                     @break
                  @endif  
                @endforeach
              @else
                <img src="/img/generic_car.png" class="img-responsive">
              @endif

              <div class="card-car-data">
                <span class="card-car-sub-data">
                  <span class="con">KM • </span>
                  {!! $auto->kilometraje !!}
                  <span class="con con-end">Año • </span>
                  {!! $auto->agno !!}
                </span>
                <h5>{!! $auto->marca !!} {!! $auto->modelo !!} {!! $auto->version !!}</h5>
                <span class="slide-cant" style="font-size: 21px">
                  <span class="sig-data">ORIGINAL: </span>
                  <span class="sig-data">$</span> 
                  <span class="normal-precio">{!! $auto->precio !!}</span>
                </span>
                <hr>
                <span class="slide-cant" style="color: #ec3636;font-size: 24px;">
                  <span class="sig-data">OFERTA: </span>
                  <span class="sig-data">$</span> 
                  <span class="precio-oferta">{!! $auto->precio_oferta !!}</span>
                </span>
              </div>
              <a href="{{ url('/auto/' . $auto->id ) }}" id="" class="">
               <button id="" class="bttn-fill bttn-md bttn-danger">
                <i class="fa fa-car"></i>
                VER FICHA
               </button>
              </a>
            </div>
          </div>

      @endforeach
    </div>

    </div>

  </div>
</div>

@stop 