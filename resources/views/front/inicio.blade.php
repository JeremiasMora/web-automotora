@extends('layouts.front')

@section('content')

<header id="main-banner">
  <h2>VENTA DE AUTOS USADOS</h2>
  <div id="sub-title" class="hidden-sm hidden-xs">
    @if(count($autosOferta) != 0)
      <h5>ÚLTIMAS OFERTAS</h5>
    @endif
  </div>
  @if(count($autosOferta) != 0)
  <a href="{{ url('/ofertas') }}" id="btn-ver-of-a" class="hidden-md hidden-lg">
   <button id="btn-ver-of" class="bttn-fill bttn-md bttn-danger">VER OFERTAS</button>
  </a>
  
  <div id="slide-ofertas">
    
   <!-- Flickity HTML init -->
    <div class="carousel hidden-sm hidden-xs">
      
      @foreach($autosOferta as $auto)
          <div class="carousel-cell">
            <div class="slide-oferta-content" style="position: relative">
              <h3>{!! $auto->marca !!} {!! $auto->modelo !!} {!! $auto->version !!}</h3>
              <span class="slide-data">
                <span class="con">KM • </span>
                {!! $auto->kilometraje !!}
                <span class="con con-end">Año • </span>
                {!! $auto->agno !!}
              </span>
              <span class="slide-cant" style="font-size: 21px">
                <span class="sig-data">ORIGINAL: </span>
                <span class="sig-data">$</span> 
                <span class="normal-precio">{!! $auto->precio !!}</span>
              </span>
              <hr>
              <span class="slide-cant" style="color: #ec3636;font-size: 24px;">
                <span class="sig-data">OFERTA: </span>
                <span class="sig-data">$</span> 
                <span class="precio-oferta">{!! $auto->precio_oferta !!}</span>
              </span>
              <a href="{{ url('/auto/' . $auto->id ) }}" id="" class="" style="display: block; padding-top: 20px; position: absolute; bottom: 20px; right: 20px; width: 100%">
               <button id="" class="bttn-fill bttn-md bttn-danger pull-right">
                VER FICHA
               </button>
              </a>
            </div>
            @if ($auto->main_image != 0)
                @foreach($auto->images as $image)
                  @if ($auto->main_image == $image->id)
                    <img src="https://s3.us-east-1.amazonaws.com/aliagautomotriz/{!! $image->path !!}" class="img-responsive">
                     @break
                  @endif  
                @endforeach
              @else
                <img src="/img/generic_car.png" class="img-responsive">
              @endif
          </div>
      @endforeach
    </div>
  </div> 
  @endif
</header>

<div id="data-cars">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h3>AUTOS EN <span>VITRINA</span></h3>
        <div class="head-icon hidden-xs">
          <img src="img/head-icon.gif" alt="">
        </div>
      </div>
    </div>

    <div id="list-cars" class="row">

      @foreach($autos as $auto)
       
          <div class="col-md-4 col-sm-6">
            <div class="card-car" style="height: 440px;">

              @if ($auto->main_image != 0)
                @foreach($auto->images as $image)
                  @if ($auto->main_image == $image->id)
                    <img src="https://s3.us-east-1.amazonaws.com/aliagautomotriz/{!! $image->path !!}" class="img-responsive">
                     @break
                  @endif  
                @endforeach
              @else
                <img src="/img/generic_car.png" class="img-responsive">
              @endif

              <div class="card-car-data">
                <span class="card-car-sub-data">
                  <span class="con">KM • </span>
                  {!! $auto->kilometraje !!}
                  <span class="con con-end">Año • </span>
                  {!! $auto->agno !!}
                </span>
                <h5>{!! $auto->marca !!} {!! $auto->modelo !!} {!! $auto->version !!}</h5>
                <p>{!! $auto->des !!}</p>
                <span class="card-cant">
                  $ <span class="normal-precio">{!! $auto->precio !!}</span>
                </span>
              </div>
              <a href="{{ url('/auto/' . $auto->id ) }}" id="" class="">
               <button id="" class="bttn-fill bttn-md bttn-danger">
                <i class="fa fa-car"></i>
                VER FICHA
               </button>
              </a>
            </div>
          </div>

      @endforeach
    </div>
    
    <div class="pull-right">
      {!! $autos->render() !!}
    </div>

  </div>
</div>

@stop 