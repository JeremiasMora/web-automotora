<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>MiguelAliaga - Login</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.13/semantic.min.css">
  <!--  -->
  @if (getenv('APP_ENV') === 'production')
    <link href="{{ secure_asset('css/styles-login.css') }}" media="all" rel="stylesheet" type="text/css" />
  @elseif (getenv('APP_ENV') === 'local')
    {!!Html::style('css/styles-login.css')!!}
  @endif
</head>
<body>
  <div class="ui middle aligned center aligned grid">
    <div class="column">
      <h2 class="ui image header">
        <div class="content">
          <h2>Bienvenido | MiguelAliaga</h2>
        </div>
      </h2>

        {!! Form::open(['route'=>'user.login', 'method' => 'POST', 'class' => 'ui large form']) !!}
          <div class="ui stacked secondary segment">

            @if (session('msg'))
              <div class="ui red message">
                  <p>{!! session('msg') !!}</p>
                </div>
            @endif

            <div class="field">
              <div class="ui left icon input">
                <i class="user icon"></i>
                {!!Form::text('username', null,['class'=>'form-control', 
              'placeholder' => 'Correo'])!!}
              </div>
            </div>

            <div class="field">
              <div class="ui left icon input">
                <i class="lock icon"></i>
                {!!Form::password('password', null,['class'=>'form-control', 
              'placeholder' => '*********'])!!}
              </div>
            </div>

              {!!Form::submit('INGRESAR', ['class'=>'ui fluid large blue submit button'])!!}

          </div>
        {!! Form::close() !!}


    </div>
  </div>


</body>
</html>