@extends('layouts.main')

@section('content')

<!-- BODY --> 
<div class="ui container">
   <br>
   <!-- BODY HEADER -->
   <div class="row">
     <div class="ui grid">
          
       <div class="eight wide column">
          <h4 class="title">Registrar nuevo automóvil</h4>
       </div>
       <div class="eight wide column"> 
       </div>
    </div>
  </div>
  <br>

  <!-- BODY TABLE -->
  <div class="row">
    <div class="ui segment">

        @if(count($errors) > 0)
            <div class="ui red message">
                <h4>Error al crear registro</h4>

                <ul>
                    @foreach($errors->all() as $error)
                        <li>{!! $error !!}</li>
                    @endforeach
                </ul>
            </div>
        @endif
      
      <h4 class="ui dividing header">Datos básicos</h4>
      {!!Form::open(['route'=>'auto.store', 'method'=>'POST', 'class' => 'ui form'])!!}
        {{ csrf_field() }}
        <div class="field">
          <div class="two fields">
            <div class="field">
              <label>Marca</label>
              {!!Form::text('marca', null,[])!!}
            </div>
            <div class="field">
              <label>Modelo</label>
              {!!Form::text('modelo', null,[])!!}
            </div>
          </div>
        </div>

        <div class="field">
          <div class="two fields">
            <div class="field">
              <label>Version</label>
              {!!Form::text('version', null,[])!!}
            </div>
            <div class="field">
              <label>Año</label>
              {!!Form::number('agno', 0,[])!!}
            </div>
          </div>
        </div>

        <div class="field">
          <label for="">Descripción</label>
          {!!Form::textarea('des', null,['rows' => 3])!!}
        </div>

        <h4 class="ui dividing header">Precio</h4>

        <div class="field">
          <label for="">Precio (sin "." y ",")</label>
          <div class="ui right labeled input">
            <label for="amount" class="ui label">$</label>
            {!!Form::number('precio', 0,[])!!}
          </div>
        </div>

      

        <div class="ui inverted segment">
          <div class="field">
            <label for="">En oferta</label>
            <div class="ui checked checkbox">
              <input type="checkbox">
              {!! Form::checkbox('en_oferta')!!}
              <label>Si</label>
            </div>
          </div>
          <div class="field">
            <label for="">Precio en oferta (sin "." y ",")</label>
            <div class="ui right labeled input">
              <label for="amount" class="ui label" style="color: #000">$</label>
              {!!Form::number('precio_oferta', 0,[])!!}
            </div>
          </div>
        </div>

        <h4 class="ui dividing header">Otros datos</h4>


        
          <div class="three fields">
            <div class="field">
              <div class="ui segment">
                <div class="field">
                  <div class="ui toggle checkbox">
                    {!! Form::checkbox('aire_acondicionado')!!}
                    <label>Aire acondicionado</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="field">
              <div class="ui segment">
                <div class="field">
                  <div class="ui toggle checkbox">
                    {!! Form::checkbox('alza_vid_elect')!!}
                    <label>Alza vidrios electrónicos</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="field">
              <div class="ui segment">
                <div class="field">
                  <div class="ui toggle checkbox">
                    {!! Form::checkbox('frenos_abs')!!}
                    <label>Frenos ABS</label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="three fields">
            <div class="field">
              <div class="ui segment">
                <div class="field">
                  <div class="ui toggle checkbox">
                    {!! Form::checkbox('airbag')!!}
                    <label>Airbag</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="field">
              <div class="ui segment">
                <div class="field">
                  <div class="ui toggle checkbox">
                    {!! Form::checkbox('cierre_centralizado')!!}
                    <label>Cierre centralizado</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="field">
              <div class="ui segment">
                <div class="field">
                  <div class="ui toggle checkbox">
                    {!! Form::checkbox('catalitico')!!}
                    <label>Catalítico</label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="three fields">
   
            <div class="field">
              <div class="ui segment">
                <div class="field">
                  <div class="ui toggle checkbox">
                    {!! Form::checkbox('espejos_elect')!!}
                    <label>Espejos electrónicos</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="field">
              <div class="ui segment">
                <div class="field">
                  <div class="ui toggle checkbox">
                    {!! Form::checkbox('unico_dueno')!!}
                    <label>Único dueño</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="field">
              <div class="ui segment">
                <div class="field">
                  <div class="ui toggle checkbox">
                    {!! Form::checkbox('ctrl_estabilidad')!!}
                    <label>Control de estabilidad</label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="three fields">

          
            <div class="field">
              <div class="ui segment">
                <div class="field">
                  <div class="ui toggle checkbox">
                    {!! Form::checkbox('radio')!!}
                    <label>Radio</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="field">
              <div class="ui segment">
                <div class="field">
                  <div class="ui toggle checkbox">
                    {!! Form::checkbox('ctrl_crucero')!!}
                    <label>Control crucero</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="field">
             
            </div>
          </div>

          <div class="three fields">
            
            <div class="field">
              
            </div>
            <div class="field">
             
            </div>
            <div class="field">
             
            </div>
          </div>

          <div class="three fields">
            <div class="field">
              <label for="">Cantidad Airbag</label>
              {!!Form::number('cant_airbag', 0,[])!!}
            </div>
            <div class="field">
              <label for="">Consumo por litro</label>
              {!!Form::number('consumo_combustible', 0,[])!!}
            </div>
            <div class="field">
              <label for="">Kilometraje</label>
              {!!Form::number('kilometraje', 0,[])!!}
            </div>
          </div>

          <div class="three fields">
            <div class="field">
              <label for="">Cilindrada</label>
              {!!Form::number('cilindrada', 0,[])!!}
            </div>
            <div class="field">
              <label for="">Combustible</label>
              {!! Form::select('combustible_id', $combustibles, null, ['class' => 'ui dropdown']) !!}
            </div>
            <div class="field">
              <label for="">Tipo</label>
              {!! Form::select('tipo_id', $tipos, null, ['class' => 'ui dropdown']) !!}
            </div>
          </div>

          <div class="row">
            <div class="ui grid">
              <div class="eight wide column">
                <a href="{{ url('app/auto') }}" class="ui button">Volver a registros</a>
              </div>
              <div class="eight wide column">
                {!!Form::submit('Crear', ['class'=>'ui blue button right floated'])!!}
              </div>
            </div>
          </div>

      {!!Form::close()!!}

    </div>
  </div><!-- /BODY TABLE -->
</div> <!-- /BODY --> 
<br>
@stop 