@extends('layouts.main')

@section('content')
<style>

  .hidden{
    display: none !important;
  }

</style>
<!-- BODY --> 
<div class="ui container">
   <br>
   <!-- BODY HEADER -->
   <div class="ui grid">
        
     <div class="eight wide column">
        <h4 class="title"><i class="icon car"></i> {!! $auto->marca .' '. $auto->modelo!!}</h4>
     </div>
     <div class="eight wide column">
     </div>

  </div>
  <br>
  
  <!-- CONTAINER: BODY TABLE -->
  <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
  <div class="ui">
    <div class="ui segment">

      <div class="ui grid">
        <div class="five wide column">
    
          <h4>Subir nuevas imágenes</h4>
          <div class="ui divider"></div>   

          
            @if (session('msg'))
              <div class="row" style="margin-bottom: 10px">
                <div class="ui positive message">
                  <p>{!! session('msg') !!}</p>
                </div>
              </div>  
            @endif

            @if(count($errors) > 0)
              <div class="row" style="margin-bottom: 10px">
                <div class="ui red message">
                    <h4>Error al subir</h4>
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{!! $error !!}</li>
                        @endforeach
                    </ul>
                </div>
              </div>
            @endif
               

          <div class="ui form">
          {!!Form::open(['route'=>'images.store', 'files' => true, 'method'=>'POST', 'class' => 'ui form'])!!}
            {!! Form::hidden('idAuto', $auto->id, ['id' => 'idAuto']) !!}

            <div>
                <label for="file" class="ui icon small button">
                    <i class="file icon"></i>
                    Seleccionar imágenes</label>
                {!! Form::file('path[]', ['id' => 'file', 'class' => 'hidden', 'accept' => '.png, .jpg, .jpeg', 'multiple' => 'true']) !!}
            </div>

            

            <div class="info-parent">
              <span id="info-input-file"></span>
            </div>
            <br>

            <div id="thumbs" class="ui small images">
              <div class="item">
                <p class="ui pre-img">Nada para mostrar</p>
              </div>
            </div>

    
            {!!Form::submit('Subir', ['class'=>'ui blue button right floated'])!!}
          {!!Form::close()!!}
          </div>

          <!--<img src="" class="ui small image"  height="200" alt="Image preview...">-->

        </div>

        <div class="eleven wide column">
          <h4>Imágenes subidas</h4>
          <div class="ui divider"></div> 

          <div class="ui divided items">

            @foreach($images as $image)
              <div class="item">
                <div class="ui small image">
                  <img src="https://s3.us-east-1.amazonaws.com/aliagautomotriz/{!! $image->path !!}">
                </div>
                <div class="middle aligned content">
                  @if ($image->id == $idMainImage)
                    <button class="ui right labeled icon green button">
                      <i class="check arrow icon"></i>
                      Principal
                    </button>
                  @else
                    <button class="ui button main-btn-img" onclick="addMain({!! $image->id !!})">
                      Principal
                    </button>
                    <button class="ui red button delete-btn-img" onclick="addDelete({!! $image->id !!})">Eliminar</button>
                  @endif
                </div>
              </div>
            @endforeach
          </div>

        </div>
      </div>  


    </div>
      
  </div><!-- /BODY TABLE -->
</div> <!-- /BODY --> 


<div class="ui mini modal" id="main-modal-img">
  <div class="ui icon header">
    <i class="warning icon"></i>
    Estas seguro?
  </div>
  <div class="content">
    Vas a establecer como imagen principal del automóvil
  </div>
  <div class="actions">
    <div class="ui red deny button">
      No
    </div>
    <div class="ui positive right button" onclick="isMainImage()">
      Si!
    </div>
  </div>
</div>

<div class="ui basic mini modal" id="delete-modal-img">
  <div class="ui icon header">
    <i class="warning icon"></i>
    Estas seguro?
  </div>
  <div class="content">
    Vas a eliminar una imagen de forma permanente.
  </div>
  <div class="actions">
    <div class="ui red deny button">
      No
    </div>
    <div class="ui positive right button" onclick="deleteImage()">
      Si, eliminar!
    </div>
  </div>
</div>

<script>
      var idDelete = null;
      var idMain = null;

      function addDelete(id){
        idDelete = id;
        console.log(idDelete);
      }

      function addMain(id){
        idMain = id;
        console.log(idMain);
      }


      function deleteImage(){
        $.ajax({
          url: '../image/' + $('#idAuto').val() + "/" + idDelete,
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          type: 'DELETE',
          success: function(result) {
              window.location.replace(""+$('#idAuto').val());
          }
        });
        
      }  

      function isMainImage(){
        $.ajax({
          url: '../image/main/' + $('#idAuto').val() + "/" + idMain,
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          type: 'POST',
          success: function(result) {
              window.location.replace(""+$('#idAuto').val());
          }
        });
        
      }   
  </script>
@stop