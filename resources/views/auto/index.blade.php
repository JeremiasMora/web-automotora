@extends('layouts.main')

@section('content')
<!-- BODY --> 
<div class="ui container">
   <br>
   <!-- BODY HEADER -->
   <div class="row">
     <div class="ui grid">
          
       <div class="eight wide column">
          <h4 class="title"><i class="icon car"></i> Automóviles registrados</h4>
       </div>
       <div class="eight wide column">

				
         <a href="{{ url('app/auto/create') }}" class="ui blue button right floated">
           <i class="icon plus"></i>
           Agregar automóvil
         </a>

         <div class="ui icon input" id="content-search">
        <input type="text" id="search-car" placeholder="Buscar...">
        <i class=" search icon"></i>
      </div>
       </div>

    </div>
  </div>
  <br>
  <div class="row">
    @if (session('msg'))
      <div class="ui positive message">
        <p>{!! session('msg') !!}</p>
      </div>
    @endif
  </div>

  <!-- BODY TABLE -->
  <div class="row">
  <input type="hidden" name="_token" value="{{ csrf_token() }}" id="token">
    <div>
      <table class="ui table" id="mainTable"">
        <thead>
          <tr>
            <th></th>
            <th>En promoción</th>
            <th>Marca</th>
            <th>Modelo</th>
            <th>Versión</th>
            <th>Año</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          @foreach($autos as $auto)
            <tr>
              <td class="td-img">
                @if ($auto->main_image != 0)
                  @foreach($auto->images as $image)
                    @if ($auto->main_image == $image->id)
                      <img src="https://s3.us-east-1.amazonaws.com/aliagautomotriz/{!! $image->path !!}" class="ui tiny image">
                       @break
                    @endif  
                  @endforeach
                @else
                  <img src="/img/generic_car.png" class="ui tiny image">
                @endif
              </td>
              <td class="td-promo">
                 <!--<div class="ui ribbon label">En promoción</div>-->
                @if ($auto->en_oferta)
                  <i class="large green checkmark icon"></i>
                @else
                  <i class="large red cancel icon"></i>
                @endif
              </td>
              
              <td>
                {!! $auto->marca !!}
              </td>
              <td>
                {!! $auto->modelo !!}
              </td>
              <td>
                {!! $auto->version !!}
              </td>
              <td>
                {!! $auto->agno !!}
              </td>
              <td>
                <a href="{{ url('/auto/' . $auto->id) }}" class="circular ui orange icon button b-popup" data-content="Visualizar" data-position="top center" data-variation="mini" target="_blank">
                   <i class="icon unhide"></i>
                </a>
                <a href="{{ url('app/auto/' . $auto->id .'/edit') }}" class="circular ui orange icon button b-popup" data-content="Editar" data-position="top center" data-variation="mini">
                  <i class="icon pencil"></i>
                </a> 
                <a href="{{ url('app/images/' . $auto->id) }}" class="circular ui orange icon button b-popup" data-content="Imágenes" data-position="top center" data-variation="mini">
                  <i class="icon photo"></i>
                </a> 
                <button class="circular ui orange icon button delete-btn b-popup" data-content="Eliminar" data-position="top center" data-variation="mini" onclick="addDelete({!! $auto->id !!})">
                  <i class="icon trash"></i>
                </button> 
              </td>
            </tr>
          @endforeach
        </tbody>
        
      </table>
      
    </div>
  </div><!-- /BODY TABLE -->
</div> <!-- /BODY --> 

<div class="ui modal fullscreen" id="see-modal">
  <div class="scrolling content">
    <p>
      
    Vista...
    </p>
  </div>
  <div class="actions">
    <div class="ui positive right labeled icon button">
      Listo
      <i class="checkmark icon"></i>
    </div>
  </div>
</div>

<div class="ui basic mini modal" id="delete-modal">
  <div class="ui icon header">
    <i class="warning icon"></i>
    Estas seguro?
  </div>
  <div class="content">
    Vas a eliminar un registro de forma permanente.
  </div>
  <div class="actions">
    <div class="ui red deny button">
      No
    </div>
    <div class="ui positive right button" onclick="deleteAuto()">
      Si, eliminar!
    </div>
  </div>
</div>

 <script>
      var idDelete = null;
      function addDelete(id){
        idDelete = id;
        
      }
      function deleteAuto(){
        $.ajax({
          url: 'auto/' + idDelete,
          headers: {'X-CSRF-TOKEN': $('#token').val()},
          type: 'DELETE',
          success: function(result) {
              window.location.replace("auto");
          }
        });
        
      }    
  </script>
@stop 